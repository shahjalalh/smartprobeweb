<?php
App::uses('AppController', 'Controller');

/**
 * Smartprobes Controller
 *
 * @property Smartprobe $Smartprobe
 * @property PaginatorComponent $Paginator
 */
class SmartprobesController extends AppController {

	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Paginator');

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this -> Smartprobe -> recursive = 0;
		$this->Paginator->settings = array(
			'limit' => 100,
	        'order'		=> array('created' => 'DESC', 'id' => 'DESC', )
	    );
		$this -> set('smartprobes', $this -> Paginator -> paginate());
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		if (!$this -> Smartprobe -> exists($id)) {
			throw new NotFoundException(__('Invalid smartprobe'));
		}
		$options = array('conditions' => array('Smartprobe.' . $this -> Smartprobe -> primaryKey => $id));
		$this -> set('smartprobe', $this -> Smartprobe -> find('first', $options));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this -> request -> is('post')) {
			$this -> Smartprobe -> create();
			if ($this -> Smartprobe -> save($this -> request -> data)) {
				$this -> Session -> setFlash(__('The smartprobe has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this -> redirect(array('action' => 'index'));
			} else {
				$this -> Session -> setFlash(__('The smartprobe could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		if (!$this -> Smartprobe -> exists($id)) {
			throw new NotFoundException(__('Invalid smartprobe'));
		}
		if ($this -> request -> is(array('post', 'put'))) {
			if ($this -> Smartprobe -> save($this -> request -> data)) {
				$this -> Session -> setFlash(__('The smartprobe has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this -> redirect(array('action' => 'index'));
			} else {
				$this -> Session -> setFlash(__('The smartprobe could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('Smartprobe.' . $this -> Smartprobe -> primaryKey => $id));
			$this -> request -> data = $this -> Smartprobe -> find('first', $options);
		}
	}

	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		$this -> Smartprobe -> id = $id;
		if (!$this -> Smartprobe -> exists()) {
			throw new NotFoundException(__('Invalid smartprobe'));
		}
		$this -> request -> onlyAllow('post', 'delete');
		if ($this -> Smartprobe -> delete()) {
			$this -> Session -> setFlash(__('The smartprobe has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this -> Session -> setFlash(__('The smartprobe could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this -> redirect(array('action' => 'index'));
	}

	public function services_clear() {
		$q = "TRUNCATE smartprobes";

		$this -> Smartprobe -> query($q);

		return $this -> redirect(array('action' => 'index', 'services' => false));
	}

	public function services_addProbeData() {
		$probeData = json_decode($this -> request -> data, true);
		$data = array();
		foreach ($probeData['data'] as $record) {
			$record = (array)$record;

			$record["date"] = date("Y-m-d", strtotime(str_replace('\\', '-', $record["date"])));
			$data[] = $record;
		}
		if(!empty($data)) {
			$this->Smartprobe->saveMany($data);
			$data = array('success' => true, 'message' => "data added", 'data' => $data);
		} else {
			$data = array('success' => false, 'message' => "datacannot be added", 'data' => '');
		}

		$this -> set('data', $data);
	}

}
