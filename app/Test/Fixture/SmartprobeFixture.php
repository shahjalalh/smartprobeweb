<?php
/**
 * SmartprobeFixture
 *
 */
class SmartprobeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'probeserialnumber' => array('type' => 'string', 'null' => false, 'length' => 100, 'key' => 'primary', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'temperaturehigh' => array('type' => 'float', 'null' => true, 'default' => null),
		'temperaturelow' => array('type' => 'float', 'null' => true, 'default' => null),
		'status1' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'status2' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'time' => array('type' => 'time', 'null' => false, 'default' => '00:00:00', 'key' => 'primary'),
		'date' => array('type' => 'date', 'null' => false, 'default' => '0000-00-00', 'key' => 'primary'),
		'trip1' => array('type' => 'integer', 'null' => true, 'default' => null),
		'trip2' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => array('id', 'probeserialnumber', 'time', 'date'), 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'probeserialnumber' => 'Lorem ipsum dolor sit amet',
			'temperaturehigh' => 1,
			'temperaturelow' => 1,
			'status1' => 'Lorem ipsum dolor sit amet',
			'status2' => 'Lorem ipsum dolor sit amet',
			'time' => '12:48:18',
			'date' => '2014-05-24',
			'trip1' => 1,
			'trip2' => 1
		),
	);

}
