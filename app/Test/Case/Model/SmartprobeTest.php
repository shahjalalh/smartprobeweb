<?php
App::uses('Smartprobe', 'Model');

/**
 * Smartprobe Test Case
 *
 */
class SmartprobeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.smartprobe'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Smartprobe = ClassRegistry::init('Smartprobe');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Smartprobe);

		parent::tearDown();
	}

}
