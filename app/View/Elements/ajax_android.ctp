<?php
$data = !empty($data) ? (array)$data : array('status' => 'fail', 'message' => 'NoDataFound', 'data' => null);

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');

 echo json_encode($data);
exit ;
?>
