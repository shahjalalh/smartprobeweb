<?php echo $this->element('header'); ?>
<div class="smartprobes index">

    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?php echo __('Smartprobes'); ?></h1>
                 <div>
                    <?php echo $this->Html->link('Clear Records', array('services' => true, 'controller' => 'smartprobes', 'action' => 'clear')); ?>
                </div>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->


    <div class="row">

        <div class="col-md-9">
            <table cellpadding="0" cellspacing="0" class="table table-striped">
                <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('ID'); ?></th>
                    <th><?php echo $this->Paginator->sort('userid'); ?></th>
                    <th><?php echo $this->Paginator->sort('created'); ?></th>
                    <th><?php echo $this->Paginator->sort('probeserialnumber'); ?></th>
                    <th><?php echo $this->Paginator->sort('temperature'); ?></th>
                    <th><?php echo $this->Paginator->sort('status'); ?></th>
                    <th><?php echo $this->Paginator->sort('Date Time'); ?></th>
                    <th><?php echo $this->Paginator->sort('trip'); ?></th>

                </tr>
                </thead>
                <tbody>
                	<?php foreach ($smartprobes as $smartprobe): ?>
					<tr>
						<td><?php echo h($smartprobe['Smartprobe']['id']); ?>&nbsp;</td>
                                                <td><?php echo h($smartprobe['Smartprobe']['user_id']); ?>&nbsp;</td>
						<td><?php echo h($smartprobe['Smartprobe']['created']); ?>&nbsp;</td>

						<td><?php echo h($smartprobe['Smartprobe']['probeSerialNumber']); ?>&nbsp;</td>
						<td>
							High: <?php echo h($smartprobe['Smartprobe']['temperature_high']); ?>
							<br>Low: <?php echo h($smartprobe['Smartprobe']['temperature_low']); ?>
						</td>
						<td>Status 1: <?php echo h($smartprobe['Smartprobe']['status_1']); ?>
							<br>Status2: <?php echo h($smartprobe['Smartprobe']['status_2']); ?>&nbsp;</td>
						<td>Date: <?php echo h($smartprobe['Smartprobe']['time']); ?>
							<br>Time: <?php echo h($smartprobe['Smartprobe']['date']); ?>&nbsp;</td>
						<td>Trip1: <?php echo h($smartprobe['Smartprobe']['trip_1']); ?>
							<br>Trip2: <?php echo h($smartprobe['Smartprobe']['trip_2']); ?>&nbsp;
						</td>

					</tr>
				<?php endforeach; ?>
                </tbody>
            </table>

            <p>
                <small><?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?></small>
            </p>

            <?php
			$params = $this->Paginator->params();
			if ($params['pageCount'] > 1) {
			?>
            <ul class="pagination pagination-sm">
                	<?php
					echo $this->Paginator->prev('&larr; Previous', array('class' => 'prev','tag' => 'li','escape' => false), '<a onclick="return false;">&larr; Previous</a>', array('class' => 'prev disabled','tag' => 'li','escape' => false));
					echo $this->Paginator->numbers(array('separator' => '','tag' => 'li','currentClass' => 'active','currentTag' => 'a'));
					echo $this->Paginator->next('Next &rarr;', array('class' => 'next','tag' => 'li','escape' => false), '<a onclick="return false;">Next &rarr;</a>', array('class' => 'next disabled','tag' => 'li','escape' => false));
				?>
            </ul>
            <?php } ?>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->


</div><!-- end containing of content -->