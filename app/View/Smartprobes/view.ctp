
<!-- ==================== TOP MENU ==================== -->

<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="brand" href="http://smart-catalog.com.au/"><strong class="brandBold">SMART</strong>CATALOG</a>
            <div class="nav pull-right">
                <form class="navbar-form">
                    <div class="input-append">
                        <input class="inp-mini inp-dark span2" type="text" placeholder="search...">
                        <span class="add-on add-on-first add-on-mini add-on-dark" id="search"><i class="icon-search"></i></span>
                        <div class="collapsibleContent">
                            <a href="#tasksContent" class="sidebar"><span class="add-on add-on-middle add-on-mini add-on-dark" id="tasks"><i class="icon-tasks"></i><span class="notifyCircle cyan">3</span></span></a>
                            <a href="#notificationsContent" class="sidebar"><span class="add-on add-on-middle add-on-mini add-on-dark" id="notifications"><i class="icon-bell-alt"></i><span class="notifyCircle orange">9</span></span></a>
                            <a href="#messagesContent" class="sidebar"><span class="add-on add-on-middle add-on-mini add-on-dark" id="messages"><i class="icon-comments-alt"></i><span class="notifyCircle red">12</span></span></a>
                            <a href="#settingsContent" class="sidebar"><span class="add-on add-on-middle add-on-mini add-on-dark" id="settings"><i class="icon-cog"></i></span></a>
                            <a href="#profileContent" class="sidebar"><span class="add-on add-on-mini add-on-dark" id="profile"><i class="icon-user"></i><span class="username">Ing. Imrich Kamarel</span></span></a>
                        </div>
                        <a href="#collapsedSidebarContent" class="collapseHolder sidebar"><span class="add-on add-on-mini add-on-dark"><i class="icon-sort-down"></i></span></a>
                    </div>
                </form>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</div>
<!-- ==================== END OF TOP MENU ==================== -->

<!-- ==================== SIDEBAR ==================== -->
<div class="hiddenContent">
<!-- ==================== SIDEBAR COLLAPSED ==================== -->
<div id="collapsedSidebarContent">
    <div class="sidebarDivider"></div>
    <div class="sidebarContent">
        <ul class="collapsedSidebarMenu">
            <li><a href="#tasksContent" class="sidebar">Tasks <div class="notifyCircle cyan">3</div><i class="icon-chevron-sign-right"></i></a></li>
            <li><a href="#notificationsContent" class="sidebar">Notifications <div class="notifyCircle orange">9</div><i class="icon-chevron-sign-right"></i></a></li>
            <li><a href="#messagesContent" class="sidebar">Messages <div class="notifyCircle red">12</div><i class="icon-chevron-sign-right"></i></a></li>
            <li><a href="#settingsContent" class="sidebar">Settings<i class="icon-chevron-sign-right"></i></a></li>
            <li><a href="#profileContent" class="sidebar">Ing. Imrich Kamarel<i class="icon-chevron-sign-right"></i></a></li>
            <li class="sublevel"><a href="#">edit profile<i class="icon-user"></i></a></li>
            <li class="sublevel"><a href="#">change password<i class="icon-lock"></i></a></li>
            <li class="sublevel"><a href="#">logout<i class="icon-off"></i></a></li>
        </ul>
    </div>
</div>
<!-- ==================== END OF SIDEBAR COLLAPSED ==================== -->

<!-- ==================== SIDEBAR TASKS ==================== -->
<div id="tasksContent">
    <div class="sidebarDivider"></div>
    <div class="sidebarContent">
        <a href="#collapsedSidebarContent" class="showCollapsedSidebarMenu"><i class="icon-chevron-sign-left"></i><h1> Tasks</h1></a>
        <h1>Tasks</h1>
        <div class="sidebarInfo">
            <div class="progressTasks"><span class="label">11</span> tasks in progress</div>
            <div class="newTasks"><span class="label cyan">3</span> new tasks</div>
        </div>
        <ul class="tasksList">
            <li class="new">
                <h3>Update database</h3>
                <span class="taskProgress">0%</span>
                <div class="progress progress-striped active">
                    <div class="bar"></div>
                </div>
                <div class="appendedTags">
                    <div class="tag priority red">High priority</div>
                    <div class="tag status cyan">New task</div>
                </div>
            </li>
            <li class="new">
                <h3>Clean CSS</h3>
                <span class="taskProgress">0%</span>
                <div class="progress progress-striped active">
                    <div class="bar"></div>
                </div>
                <div class="appendedTags">
                    <div class="tag priority orange">Normal priority</div>
                    <div class="tag status cyan">New task</div>
                </div>
            </li>
            <li class="new">
                <h3>Clean JavaScript</h3>
                <span class="taskProgress">0%</span>
                <div class="progress progress-striped active">
                    <div class="bar"></div>
                </div>
                <div class="appendedTags">
                    <div class="tag priority green">Low priority</div>
                    <div class="tag status cyan">New task</div>
                </div>
            </li>
            <li>
                <h3>Make a backup</h3>
                <span class="taskProgress">75%</span>
                <div class="progress progress-striped active">
                    <div class="bar" style="width: 75%;"></div>
                </div>
                <div class="appendedTags">
                    <div class="tag priority green">Low priority</div>
                </div>
            </li>
            <li>
                <h3>Clean HTML</h3>
                <span class="taskProgress">50%</span>
                <div class="progress progress-striped active">
                    <div class="bar" style="width: 50%;"></div>
                </div>
                <div class="appendedTags">
                    <div class="tag priority red">High priority</div>
                </div>
            </li>
            <li>
                <h3>Make a coffee</h3>
                <span class="taskProgress">25%</span>
                <div class="progress progress-striped active">
                    <div class="bar" style="width: 25%;"></div>
                </div>
                <div class="appendedTags">
                    <div class="tag priority orange">Normal priority</div>
                </div>
            </li>
            <li>
                <h3>THEKAMAREL project</h3>
                <span class="taskProgress">100%</span>
                <div class="progress progress-striped">
                    <div class="bar" style="width: 100%;"></div>
                </div>
                <div class="appendedTags">
                    <div class="tag priority red">High priority</div>
                    <div class="tag status grey">Finished task</div>
                </div>
            </li>
        </ul>
        <button class="btn btn-primary">View all</button>
    </div>
</div>
<!-- ==================== END OF SIDEBAR TASKS ==================== -->

<!-- ==================== SIDEBAR NOTIFICATIONS ==================== -->
<div id="notificationsContent">
    <div class="sidebarDivider"></div>
    <div class="sidebarContent">
        <a href="#collapsedSidebarContent" class="showCollapsedSidebarMenu"><i class="icon-chevron-sign-left"></i><h1> Notifications</h1></a>
        <h1>Notifications</h1>
        <div class="sidebarInfo">
            <div><span class="label">32</span> notifications</div>
            <div><span class="label orange">9</span> new notifications</div>
        </div>
        <ul class="notificationsList">
            <li class="new first">
                <div><i class="icon-user"></i> New user registered</div>
                <span class="notificationDate">1 minute ago <span class="pull-right notificationStatus">new</span></span>
            </li>
            <li class="new">
                <div><i class="icon-user"></i> New user registered</div>
                <span class="notificationDate">3 minutes ago <span class="pull-right notificationStatus">new</span></span>
            </li>
            <li class="new">
                <div><i class="icon-comments"></i> New comment</div>
                <span class="notificationDate">15 minutes ago <span class="pull-right notificationStatus">new</span></span>
            </li>
            <li class="new">
                <div><i class="icon-shopping-cart"></i> New order</div>
                <span class="notificationDate">23 minutes ago <span class="pull-right notificationStatus">new</span></span>
            </li>
            <li class="new">
                <div><i class="icon-shopping-cart"></i> Order cancelled</div>
                <span class="notificationDate">1 hour ago <span class="pull-right notificationStatus">new</span></span>
            </li>
            <li class="new">
                <div><i class="icon-comments"></i> New comment</div>
                <span class="notificationDate">1 hour ago <span class="pull-right notificationStatus">new</span></span>
            </li>
            <li class="new">
                <div><i class="icon-user"></i> New user registered</div>
                <span class="notificationDate">3 hours ago <span class="pull-right notificationStatus">new</span></span>
            </li>
            <li class="new">
                <div><i class="icon-user"></i> New user registered</div>
                <span class="notificationDate">5 hours ago <span class="pull-right notificationStatus">new</span></span>
            </li>
            <li class="new">
                <div><i class="icon-user"></i> User account cancelled</div>
                <span class="notificationDate">6 hours ago <span class="pull-right notificationStatus">new</span></span>
            </li>
            <li>
                <div><i class="icon-comments"></i> New comment</div>
                <span class="notificationDate">10 hour ago</span>
            </li>
            <li>
                <div><i class="icon-shopping-cart"></i> New order</div>
                <span class="notificationDate">23 hours ago</span>
            </li>
            <li>
                <div><i class="icon-comments"></i> New comment</div>
                <span class="notificationDate">yesterday</span>
            </li>
            <li>
                <div><i class="icon-shopping-cart"></i> New order</div>
                <span class="notificationDate">yesterday</span>
            </li>
            <li>
                <div><i class="icon-shopping-cart"></i> Order cancelled</div>
                <span class="notificationDate">yesterday</span>
            </li>
            <li>
                <div><i class="icon-comments"></i> New comment</div>
                <span class="notificationDate">01.03.2013</span>
            </li>
            <li>
                <div><i class="icon-user"></i> New user registered</div>
                <span class="notificationDate">01.03.2013</span>
            </li>
            <li>
                <div><i class="icon-user"></i> New user registered</div>
                <span class="notificationDate">02.03.2013</span>
            </li>
            <li>
                <div><i class="icon-user"></i> User account cancelled</div>
                <span class="notificationDate">02.03.2013</span>
            </li>
            <li>
                <div><i class="icon-shopping-cart"></i> New order</div>
                <span class="notificationDate">02.03.2013</span>
            </li>
            <li>
                <div><i class="icon-shopping-cart"></i> Order cancelled</div>
                <span class="notificationDate">03.03.2013</span>
            </li>
        </ul>
        <button class="btn btn-primary">View all</button>
    </div>
</div>
<!-- ==================== END OF SIDEBAR NOTIFICATIONS ==================== -->

<!-- ==================== SIDEBAR MESSAGES ==================== -->
<div id="messagesContent">
    <div class="sidebarDivider"></div>
    <div class="sidebarContent">
        <a href="#collapsedSidebarContent" class="showCollapsedSidebarMenu"><i class="icon-chevron-sign-left"></i><h1> Messages</h1></a>
        <h1>Messages</h1>
        <div class="sidebarInfo">
            <div><span class="label">128</span> messages</div>
            <div><span class="label red">12</span> unreaded messages</div>
        </div>
        <ul class="messagesList">
            <li class="unreaded">
                <div class="messageAvatar"><img src="img/rimmer-avatar.jpg" alt=""></div>
                <h3>Arnold Karlsberg</h3>
                <span class="messageDate">05.03.2013 17:55 <span class="pull-right messageStatus">unreaded</span></span>
                <div class="messageContent">"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad..."</div>
            </li>
            <li class="unreaded">
                <div class="messageAvatar"><img src="img/homer-avatar.jpg" alt=""></div>
                <h3>John Doe</h3>
                <span class="messageDate">05.03.2013 17:55 <span class="pull-right messageStatus">unreaded</span></span>
                <div class="messageContent">"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad..."</div>
            </li>
            <li class="unreaded">
                <div class="messageAvatar"><img src="img/peter-avatar.jpg" alt=""></div>
                <h3>Peter Kay</h3>
                <span class="messageDate">05.03.2013 17:55 <span class="pull-right messageStatus">unreaded</span></span>
                <div class="messageContent">"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad..."</div>
            </li>
            <li class="unreaded">
                <div class="messageAvatar"><img src="img/zoidberg-avatar.jpg" alt=""></div>
                <h3>George McCain</h3>
                <span class="messageDate">05.03.2013 17:55 <span class="pull-right messageStatus">unreaded</span></span>
                <div class="messageContent">"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad..."</div>
            </li>
            <li class="unreaded">
                <div class="messageAvatar"><img src="img/peter-avatar.jpg" alt=""></div>
                <h3>Peter Kay</h3>
                <span class="messageDate">05.03.2013 17:55 <span class="pull-right messageStatus">unreaded</span></span>
                <div class="messageContent">"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad..."</div>
            </li>
            <li class="unreaded">
                <div class="messageAvatar"><img src="img/rimmer-avatar.jpg" alt=""></div>
                <h3>Arnold Karlsberg</h3>
                <span class="messageDate">05.03.2013 17:55 <span class="pull-right messageStatus">unreaded</span></span>
                <div class="messageContent">"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad..."</div>
            </li>
        </ul>
        <button class="btn btn-primary">View all</button>
    </div>
</div>
<!-- ==================== END OF SIDEBAR MESSAGES ==================== -->

<!-- ==================== SIDEBAR SETTINGS ==================== -->
<div id="settingsContent">
    <div class="sidebarDivider"></div>
    <div class="sidebarContent">
        <a href="#collapsedSidebarContent" class="showCollapsedSidebarMenu"><i class="icon-chevron-sign-left"></i><h1> Settings</h1></a>
        <h1>Settings</h1>
        <ul class="settingsList">
            <li>
                <div class="settingSlider">
                    <i class="icon-leaf"></i><h3> Slider</h3>
                    <div class="sl1"></div>
                </div>
            </li>
            <li>
                <div class="settingSlider">
                    <i class="icon-leaf"></i><h3> RangeSlider</h3>
                    <div class="sl2"></div>
                </div>
            </li>
            <li>
                <div class="settingToggler">
                    <i class="icon-leaf"></i><h3> Toggler</h3>
                    <label class="checkbox toggle well" onClick="">
                        <input id="toggler" type="checkbox" checked />
                        <span class="slider-selection"></span>
                                    <span class="toggleStatus">
                                        <span><i class="icon-ok toggleOn"></i></span>
                                        <span><i class="icon-remove toggleOff"></i></span>
                                    </span>
                        <a class="btn btn-primary slide-button"></a>
                    </label>
                </div>
            </li>

            <li class="fontSize">
                <h2>Font Size</h2>
                <div class="fontSizeBlock fontSizeSmall active"><span>A</span></div>
                <div class="fontSizeBlock fontSizeMedium"><span>A</span></div>
                <div class="fontSizeBlock fontSizeBig"><span>A</span></div>
            </li>

            <li class="fontStyle">
                <h2>Font Style</h2>
                <div class="fontStyleBlock fontStyleSansSerif active"><span>Aa</span></div>
                <div class="fontStyleBlock fontStyleSerif"><span>Aa</span></div>
            </li>

        </ul>
    </div>
</div>
<!-- ==================== END OF SIDEBAR SETTINGS ==================== -->

<!-- ==================== SIDEBAR PROFILE ==================== -->
<div id="profileContent">
    <div class="sidebarDivider"></div>
    <div class="sidebarContent">
        <a href="#collapsedSidebarContent" class="showCollapsedSidebarMenu"><i class="icon-chevron-sign-left"></i><h1> My account</h1></a>
        <h1>My account</h1>
        <div class="profileBlock">
            <div class="profilePhoto">
                <div class="usernameHolder">Ing. Imrich Kamarel</div>
            </div>
            <div class="profileInfo">
                <p><i class="icon-map-marker"></i> Piestany, SK</p>
                <p><i class="icon-envelope-alt"></i> ici.kamarel@tattek.com</p>
                <p><i class="icon-globe"></i> tattek.com</p>
                <p class="aboutMe">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </p>
            </div>
            <footer>
                <div class="profileSettingBlock editProfile"><i class="icon-user"></i>edit profile</div>
                <div class="profileSettingBlock changePassword"><i class="icon-lock"></i>change password</div>
                <div class="profileSettingBlock logout"><i class="icon-off"></i>logout</div>
            </footer>
        </div>
    </div>
</div>
<!-- ==================== END OF SIDEBAR PROFILE ==================== -->

</div>
<!-- ==================== END OF SIDEBAR ==================== -->

<!-- ==================== MAIN MENU ==================== -->
<div class="mainmenu">
    <div class="container-fluid">
        <ul class="nav">
            <li class="collapseMenu"><a href="#"><i class="icon-double-angle-left"></i>hide menu<i class="icon-double-angle-right deCollapse"></i></a></li>
            <li class="divider-vertical firstDivider"></li>
            <li class="left-side active" id="dashboard"><a href="index-2.html"><i class="icon-dashboard"></i> DASHBOARD</a></li>
            <li class="divider-vertical"></li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="formElements"><i class="icon-list"></i> FORMS <span class="label label-pressed">3</span></a>
                <ul class="dropdown-menu">
                    <li><a tabindex="-1" href="#">COMMON ELEMENTS</a></li>
                    <li><a tabindex="-1" href="#">VALIDATION</a></li>
                    <li><a tabindex="-1" href="#">WIZARD</a></li>
                </ul>
            </li>
            <li class="divider-vertical"></li>
            <li id="interfaceElements"><a href="#"><i class="icon-pencil"></i> UI ELEMENTS</a></li>
            <li class="divider-vertical"></li>
            <li id="buttonsIcons"><a href="#"><i class="icon-tint"></i> BUTTONS & ICONS</a></li>
            <li class="divider-vertical"></li>
            <li id="gridLayout"><a href="#"><i class="icon-th"></i> GRID LAYOUT</a></li>
            <li class="divider-vertical"></li>
            <li id="tables"><a href="#"><i class="icon-th-large"></i> TABLES</a></li>
            <li class="divider-vertical"></li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="samplePages"><i class="icon-globe"></i> Actions <span class="label label-pressed">4</span></a>
                <ul class="dropdown-menu">

                    <!-- Delete This Block -->
                    <!--<li><a tabindex="-1" href="#">LOGIN PAGE</a></li>
                    <li><a tabindex="-1" href="#">CALENDAR</a></li>
                    <li><a tabindex="-1" href="#">PAGE 404</a></li>
                    <li><a tabindex="-1" href="#">GALLERY</a></li>-->
                    <!-- Delete This Block -->


                    <!-- Action Menu Items Links -->
                    		<li><?php echo $this->Html->link(__('<i class="icon-edit"></i>&nbsp&nbsp;Edit Smartprobe'), array('action' => 'edit', $smartprobe['Smartprobe']['id']), array('escape' => false)); ?> </li>
		<li><?php echo $this->Form->postLink(__('<i class="icon-remove"></i>&nbsp;&nbsp;Delete Smartprobe'), array('action' => 'delete', $smartprobe['Smartprobe']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $smartprobe['Smartprobe']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('<i class="icon-list"></i>&nbsp&nbsp;List Smartprobes'), array('action' => 'index'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<i class="icon-plus"></i>&nbsp&nbsp;New Smartprobe'), array('action' => 'add'), array('escape' => false)); ?> </li>

                    <!-- Action Menu Items Links -->
                </ul>
            </li>
            <li class="divider-vertical"></li>

        </ul>
    </div>
</div>
<!-- ==================== END OF MAIN MENU ==================== -->

<!-- ==================== DROPDOWN MENU FOR MOREOPTIONS ICON (hidden state) ==================== -->
<ul class="dropdown-menu" id="moreOptionsMenu">
    <li class="dropdown-submenu">
        <a href="#">More options</a>
        <ul class="dropdown-menu">
            <li><a href="#">Second level link</a></li>
            <li><a href="#">Second level link</a></li>
            <li><a href="#">Second level link</a></li>
            <li><a href="#">Second level link</a></li>
            <li><a href="#">Second level link</a></li>
        </ul>
    </li>
    <li><a href="#">Another action</a></li>
    <li><a href="#">Something else here</a></li>
</ul>
<!-- ==================== END OF DROPDOWN MENU ==================== -->



<!-- ==================== TITLE LINE FOR HEADLINE ==================== -->
<div class="titleLine">
    <div class="container-fluid">
        <div class="titleIcon"><i class="icon-dashboard"></i></div>
        <ul class="titleLineHeading">
            <li class="heading"><h1>Dashboard</h1></li>
            <li class="subheading">the place for everything</li>
        </ul>

    </div>
</div>
<!-- ==================== END OF TITLE LINE ==================== -->

<!-- ==================== BREADCRUMBS / DATETIME ==================== -->
<ul class="breadcrumb">
    <li><i class="icon-home"></i><a href="index-2.html"> Home</a> <span class="divider"><i
                class="icon-angle-right"></i></span></li>
    <li class="active">Dashboard</li>
    <li class="moveDown pull-right">
        <span class="time"></span>
        <span class="date"></span>
    </li>
</ul>
<!-- ==================== END OF BREADCRUMBS / DATETIME ==================== -->




<div class="smartprobes view">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1><?php echo __('Smartprobe'); ?></h1>
			</div>
		</div>
	</div>

	<div class="row">


		<div class="col-md-9">			
			<table cellpadding="0" cellspacing="0" class="table table-striped">
				<tbody>
				<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($smartprobe['Smartprobe']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Probeserialnumber'); ?></th>
		<td>
			<?php echo h($smartprobe['Smartprobe']['probeserialnumber']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Temperaturehigh'); ?></th>
		<td>
			<?php echo h($smartprobe['Smartprobe']['temperaturehigh']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Temperaturelow'); ?></th>
		<td>
			<?php echo h($smartprobe['Smartprobe']['temperaturelow']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Status1'); ?></th>
		<td>
			<?php echo h($smartprobe['Smartprobe']['status1']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Status2'); ?></th>
		<td>
			<?php echo h($smartprobe['Smartprobe']['status2']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Time'); ?></th>
		<td>
			<?php echo h($smartprobe['Smartprobe']['time']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Date'); ?></th>
		<td>
			<?php echo h($smartprobe['Smartprobe']['date']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Trip1'); ?></th>
		<td>
			<?php echo h($smartprobe['Smartprobe']['trip1']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Trip2'); ?></th>
		<td>
			<?php echo h($smartprobe['Smartprobe']['trip2']); ?>
			&nbsp;
		</td>
</tr>
				</tbody>
			</table>

		</div><!-- end col md 9 -->

	</div>
</div>

