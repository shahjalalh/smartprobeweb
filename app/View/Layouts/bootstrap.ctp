<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->

<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title><?php echo $title_for_layout; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

    <!-- <link rel="stylesheet" href="css/styles.css"> -->
    <?php echo $this->Html->css('bootstrap-responsive.min'); ?>
    <?php echo $this->Html->css('styles'); ?>

    <!-- Latest compiled and minified JavaScript -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js">

    <?php
    echo $this->Html->meta('icon');

    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    ?>



    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <?php echo $this->Html->css('font-awesome.min'); ?>
    <?php //echo $this->Html->css('cake.generic'); ?>
    <?php echo $this->Html->css('cakegen'); ?>
</head>
<body class="dashboard">
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to
    improve your experience.</p>
<![endif]-->


<?php //echo $this->Element('thekamarelnavigation'); ?>


<!-- ==================== PAGE CONTENT ==================== -->
<div class="content">


    <!-- ==================== TITLE LINE FOR HEADLINE ====================
    <div class="titleLine">
        <div class="container-fluid">
            <div class="titleIcon"><i class="icon-dashboard"></i></div>
            <ul class="titleLineHeading">
                <li class="heading"><h1>Dashboard</h1></li>
                <li class="subheading">the place for everything</li>
            </ul>

        </div>
    </div>
    <!-- ==================== END OF TITLE LINE ====================

    <!-- ==================== BREADCRUMBS / DATETIME ====================
    <ul class="breadcrumb">
        <li><i class="icon-home"></i><a href="index-2.html"> Home</a> <span class="divider"><i
                    class="icon-angle-right"></i></span></li>
        <li class="active">Dashboard</li>
        <li class="moveDown pull-right">
            <span class="time"></span>
            <span class="date"></span>
        </li>
    </ul>
    <!-- ==================== END OF BREADCRUMBS / DATETIME ==================== -->

    <?php echo $this->Session->flash(); ?>

    <!-- ==================== START OF CONTENT  ==================== -->

    <?php echo $this->fetch('content'); ?>

    <!-- ==================== END OF CONTENT  ==================== -->


</div>
<!-- ==================== END OF PAGE CONTENT ==================== -->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script src="js/vendor/bootstrap-slider.js"></script>
<!-- bootstrap slider plugin -->
<script src="js/vendor/jquery.sparkline.min.js"></script>
<!-- small charts plugin -->
<script src="js/vendor/jquery.flot.min.js"></script>
<!-- charts plugin -->
<script src="js/vendor/jquery.flot.resize.min.js"></script>
<!-- charts plugin / resizing extension -->
<script src="js/vendor/jquery.flot.pie.min.js"></script>
<!-- charts plugin / pie chart extension -->
<script src="js/vendor/wysihtml5-0.3.0_rc2.min.js"></script>
<!-- wysiwyg plugin -->
<script src="js/vendor/bootstrap-wysihtml5-0.0.2.min.js"></script>
<!-- wysiwyg plugin / bootstrap extension -->
<script src="js/vendor/bootstrap-tags.js"></script>
<!-- multivalue input tags -->
<script src="js/vendor/jquery.tablesorter.min.js"></script>
<!-- tablesorter plugin -->
<script src="js/vendor/jquery.tablesorter.widgets.min.js"></script>
<!-- tablesorter plugin / widgets extension -->
<script src="js/vendor/jquery.tablesorter.pager.min.js"></script>
<!-- tablesorter plugin / pager extension -->
<script src="js/vendor/raphael.2.1.0.min.js"></script>
<!-- vector graphic plugin / for justgage.js -->
<script src="js/vendor/jquery.animate-shadow-min.js"></script>
<!-- shadow animation plugin -->
<script src="js/vendor/justgage.js"></script>
<!-- justgage plugin -->
<script src="js/vendor/bootstrap-multiselect.js"></script>
<!-- multiselect plugin -->
<script src="js/vendor/bootstrap-datepicker.js"></script>
<!-- datepicker plugin -->
<script src="js/vendor/bootstrap-colorpicker.js"></script>
<!-- colorpicker plugin -->
<script src="js/vendor/parsley.min.js"></script>
<!-- parsley validator plugin -->
<script src="js/vendor/formToWizard.js"></script>
<!-- form wizard plugin -->

<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/vendor/bootstrap-editable.min.js"></script>
<!-- editable fields plugin -->
<script src="js/thekamarel.min.js"></script>
<!-- main project js file -->

<script>
    $(document).ready(function () {
        $(".reorderAction").click(function () {
            $(".collapsibleActionHide").toggle();
        });
    });
</script>

</body>
</html>

